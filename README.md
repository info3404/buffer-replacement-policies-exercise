# README #

This is the skeleton code you will use for the Week 2 PASTA homework tasks.

Inside you will find a number of Java files. You are most concerned with buffer.\* and buffer.replacement.\*

You can play with accessing pages using the different policies by compiling and running the BufferManagerDemo class.

To submit, zip up the src folder and upload it to PASTA (see course site for URL). If you know how to run ant build scripts from you IDE you can use the build.xml file to automate the creation of the zip.

## Suggestions ##
Look at the example RandomReplacerTest class and follow the tutorial exercise on MRU.

## Questions to keep in mind ##

* What is the difference between LRU and CLOCK?
* Why would we use CLOCK over GLOCK?
* Are any of these algorithms guaranteed to give us the optimal result?
* Why is having a "good" Buffer Policy important for a Database System?