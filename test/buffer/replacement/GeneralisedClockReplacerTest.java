package buffer.replacement;

import buffer.BufferFrame;
import buffer.replacement.GeneralisedClockReplacer;
import buffer.replacement.Replacer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Unit tests for Hard task
 * To get these tests to pass you will need to fully implement GeneralisedClockReplacementPolicy
 * (@see GeneralisedClockReplacementPolicy)
 */
public class GeneralisedClockReplacerTest {

	private static final int CLOCKCOUNTLIMIT = 4;
	private static final int POOLSIZE = 5;
	private Replacer replacer;
	private List<BufferFrame> pool;
	private HashMap<BufferFrame,String> labels; // Label for each buffer frame

	@Before
	public void setUp() throws Exception {
		replacer = new GeneralisedClockReplacer(CLOCKCOUNTLIMIT);
		// Always start off with a pool of 5 frames    	
		pool = TestUtils.generateBufferFrameList(POOLSIZE);

		// NOTE: Changed so that we deal with frame labels rather
		// than addresses in the tests
		labels = new HashMap<BufferFrame,String>();
		labels.put(pool.get(0), "A");
		labels.put(pool.get(1), "B");
		labels.put(pool.get(2), "C");
		labels.put(pool.get(3), "D");
		labels.put(pool.get(4), "E");
	}

	@After
	public void testNoChangeToList() {
		assertEquals("Pool should not change in size",POOLSIZE, pool.size());
	}

	/*
	 * These tests check behaviour when there is a clear choice of page based 
	 * upon access count.
	 */

	@Test
	public void testChooseWhenOneFrameUnused() throws Exception {
		// Access all pages except the middle (C)
		TestUtils.notifyMany(replacer, pool, 1, 1, 0, 1, 1);
		BufferFrame chosen = replacer.choose(pool);
		assertEquals("Chosen frame should be the one containing the unused page", "C", labels.get(chosen));
	}

	@Test
	public void testChooseLeastUsedFrame() throws Exception {
		// Access all pages, but one page less than the others
		TestUtils.notifyMany(replacer, pool, 2, 1, 2, 2, 2);
		BufferFrame chosen = replacer.choose(pool);
		assertEquals("Chosen frame should be the one containing the least-used page", "B", labels.get(chosen));
	}


	@Test
	public void testChooseLeastUsedFrameAtCap() throws Exception {
		// Try near boundary value of count limit
		TestUtils.notifyMany(replacer, pool, CLOCKCOUNTLIMIT, CLOCKCOUNTLIMIT, CLOCKCOUNTLIMIT, CLOCKCOUNTLIMIT-1, CLOCKCOUNTLIMIT);
		BufferFrame chosen = replacer.choose(pool);
		assertEquals("Chosen frame should be the one containing the least-used page", "D", labels.get(chosen));
	}

	/*
	 * These tests expect the the clock hand to progress one after choosing a page
	 */

	@Test
	public void testMoveToNextFrameAfterChosen() throws Exception {
		BufferFrame chosen;
		int newPageId = POOLSIZE +1;
		TestUtils.notifyMany(replacer, pool, 0, 0, 0, 0, 0);

		assertEquals("First chosen frame should be the first in the pool", "A", labels.get(chosen = replacer.choose(pool)));
		TestUtils.simulatePageLoad(replacer, pool, chosen, newPageId++);

		assertEquals("Second chosen frame should be the second in the pool", "B", labels.get(chosen = replacer.choose(pool)));
		TestUtils.simulatePageLoad(replacer, pool, chosen, newPageId++);

		assertEquals("Third chosen frame should be the third in the pool", "C", labels.get(chosen = replacer.choose(pool)));
	}

	@Test
	public void testChooseLeastUsedFrameSequential() throws Exception {
		BufferFrame chosen;
		int newPageId = POOLSIZE +1;
		// Check that counters are reduced when the hand sweeps round
		// Start with a range of values, and keep replacing pages.
		TestUtils.notifyMany(replacer, pool, 3, 2, 1, 0, 4);

		// At each of the following, assert the chosen frame based upon the expected state
		assertEquals("Hand should point to A, counts should be {3, 2, 1, 0, 4}", "D", labels.get(chosen = replacer.choose(pool)));

		TestUtils.simulatePageLoad(replacer, pool, chosen, newPageId++);
		assertEquals("Hand should point to E, counts should be {2, 1, 0, 1, 4}", "C", labels.get(chosen = replacer.choose(pool)));

		TestUtils.simulatePageLoad(replacer, pool, chosen, newPageId++);
		assertEquals("Hand should point to D, counts should be {1, 0, 1, 1, 3}", "B", labels.get(chosen = replacer.choose(pool)));

		TestUtils.simulatePageLoad(replacer, pool, chosen, newPageId++);
		assertEquals("Hand should point to C, counts should be {0, 1, 1, 0, 2}", "D", labels.get(chosen = replacer.choose(pool)));

		TestUtils.simulatePageLoad(replacer, pool, chosen, newPageId++);
		assertEquals("Hand should point to E, counts should be {0, 1, 0, 1, 2}", "A", labels.get(chosen = replacer.choose(pool)));

		TestUtils.simulatePageLoad(replacer, pool, chosen, newPageId++);
		assertEquals("Hand should point to E, counts should be {1, 1, 0, 1, 1}", "C", labels.get(chosen = replacer.choose(pool)));
	}

	/*
	 * These tests expect the access counts to be capped at the specified limit 
	 */

	@Test
	public void testClockLimit() throws Exception {
		BufferFrame chosen;
		int newPageId = POOLSIZE +1;
		// Access all pages beyond the limit
		TestUtils.notifyMany(replacer, pool, CLOCKCOUNTLIMIT+4, CLOCKCOUNTLIMIT+3, CLOCKCOUNTLIMIT+2, CLOCKCOUNTLIMIT+1, CLOCKCOUNTLIMIT);
		chosen = replacer.choose(pool);
		assertEquals("All pages have been accessed up to the policy limit, so chosen page should just be first in pool", "A", labels.get(chosen));

		TestUtils.simulatePageLoad(replacer, pool, chosen, newPageId++);
		// Saturate counts on all pages
		TestUtils.notifyMany(replacer, pool, CLOCKCOUNTLIMIT, CLOCKCOUNTLIMIT, CLOCKCOUNTLIMIT, CLOCKCOUNTLIMIT, CLOCKCOUNTLIMIT);

		// Trigger the clock hand to move round and reduce the counts on each frame
		chosen = replacer.choose(pool); 
		assertEquals("All pages have been accessed up to the policy limit, so chosen page should just be next in pool", "B", labels.get(chosen));
		TestUtils.simulatePageLoad(replacer, pool, chosen, newPageId++);

		// Cause all but E's count to go back up again.
		TestUtils.notifyMany(replacer, pool, 1, 1, 1, 1, 0);
		chosen = replacer.choose(pool);
		assertEquals("Chosen frame should be the one containing the least-used page", "E", labels.get(chosen));
	}

	/*
	 * These tests check behaviour when some pages are pinned
	 */


	@Test
	public void testChooseAllFramesPinned() {
		for(BufferFrame frame: pool) {
			frame.pin();
		}

		boolean exceptionThrown = false;
		try {
			replacer.choose(pool);
		} catch (BufferFrame.AllBufferFramesPinnedException e) {
			exceptionThrown = true;
		}

		assertTrue("Choosing a page to replace when all pages pinned should throw an exception", exceptionThrown);
	}


	@Test
	public void testIncrementsWhenPinned() throws Exception {
		TestUtils.notifyMany(replacer, pool, 1, 1, 1, 0, 1);
		// Choosing now would return D - but lets access more whilst all pages pinned
		
		// Pin all pages
		for(BufferFrame frame : pool) {
			frame.pin();
		}
		TestUtils.notifyMany(replacer, pool, 3, 3, 0, 4, 3);
		// Counts should now be {4, 4, 1, 4, 4}
		// Unpin all pages
		for(BufferFrame frame : pool) {
			frame.unpin();
		}
		assertEquals("Choice takes account of frame accesses whilst pages pinned", "C", labels.get(replacer.choose(pool)));
	}

	@Test
	public void testNotDecrementsWhenPinned() throws Exception {
		BufferFrame chosen;
		TestUtils.notifyMany(replacer, pool, 2, 1, 1, 2, 0);
		
		BufferFrame B = pool.get(1);
		B.pin();
		assertEquals("Fifth page (E) selected as lowest count", "E", labels.get(chosen = replacer.choose(pool)));
		TestUtils.simulatePageLoad(replacer, pool, chosen, POOLSIZE +1);
		// A, C and D's counts should have been decremented, but not B's
		B.unpin();
		
		assertEquals("Second page (B) shouldn't be selected as count should be unchanged - C should be chosen instead", "C", labels.get(replacer.choose(pool)));
	}
	@Test
	public void testChooseCountdownWithPin() throws Exception {
		BufferFrame chosen;
		int newPageId = POOLSIZE +1;
		
		// Repeatedly replace pages, pinning each new page so it isn't considered
		// for replacement in subsequent rounds
		TestUtils.notifyMany(replacer, pool, 4, 3, 2, 1, 1);
		assertEquals("First frame to get count reduced to zero should be D(4th)","D", labels.get(chosen = replacer.choose(pool)));

		TestUtils.simulatePageLoad(replacer, pool, chosen, newPageId++);
		chosen.pin();
		assertEquals("Second frame to get count reduced to zero should be E(5th)", "E", labels.get(chosen = replacer.choose(pool)));

		TestUtils.simulatePageLoad(replacer, pool, chosen, newPageId++);
		chosen.pin();
		assertEquals("Third frame to get count reduced to zero should be C(3rd)", "C", labels.get(chosen = replacer.choose(pool)));

		TestUtils.simulatePageLoad(replacer, pool, chosen, newPageId++);
		chosen.pin();
		assertEquals("Fourth frame to get count reduced to zero should be B(2nd)", "B", labels.get(chosen = replacer.choose(pool)));

		TestUtils.simulatePageLoad(replacer, pool, chosen, newPageId++);
		chosen.pin();
		assertEquals("Final frame to get count reduced to zero should be A(1st)", "A", labels.get(chosen = replacer.choose(pool)));
	}

}