package buffer.replacement;

import buffer.BufferFrame;
import buffer.replacement.RandomReplacer;
import buffer.replacement.Replacer;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class RandomReplacerTest {

    private Replacer replacer;
    private List<BufferFrame> pool;

    @Before
    public void setUp() throws Exception {
        replacer = new RandomReplacer();
        pool = TestUtils.generateBufferFrameList(10);
    }

    @Test
    public void testRandomChoose() throws Exception {
        boolean isRandomPolicy = false;
        for(int i = 0; i < 20; i++) {
            BufferFrame first = replacer.choose(pool);
            BufferFrame second = replacer.choose(pool);
            if(first != null && second != null && !first.equals(second)) {
                isRandomPolicy = true;
            }
        }
        assertTrue(isRandomPolicy);
    }
}