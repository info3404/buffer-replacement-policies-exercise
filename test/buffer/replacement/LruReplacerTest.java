package buffer.replacement;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import buffer.BufferFrame;
import buffer.replacement.LruReplacer;
import buffer.replacement.Replacer;

/**
 * Unit tests for Easy task
 * To get these tests to pass you will need to fully implement LruReplacementPolicy
 * (@see LruReplacementPolicy)
 */
public class LruReplacerTest {

    private static final int NUMFRAMES = 5;
	private Replacer mReplacementPolicy;
    private List<BufferFrame> mPool;
    private BufferFrame A, B, C, D, E;

    @Before
    public void setUp() throws Exception {
        mReplacementPolicy = new LruReplacer();
        mPool = TestUtils.generateBufferFrameList(NUMFRAMES);
        A = mPool.get(0);
        B = mPool.get(1);
        C = mPool.get(2);
        D = mPool.get(3);
        E = mPool.get(4);
    }

    @Test
    public void testChooseWhenOneFrameUnused() throws Exception {
        mReplacementPolicy.notify(mPool, A);
        mReplacementPolicy.notify(mPool, B);
        mReplacementPolicy.notify(mPool, D);
        mReplacementPolicy.notify(mPool, E);
        // Test 4/5 used, but one never used
        assertEquals("Should get least recently used page even when it is unused",C, mReplacementPolicy.choose(mPool));
    }

    @Test
    public void testChooseNormal() throws Exception {
        mReplacementPolicy.notify(mPool, A);
        mReplacementPolicy.notify(mPool, B);
        mReplacementPolicy.notify(mPool, D);
        mReplacementPolicy.notify(mPool, E);
        mReplacementPolicy.notify(mPool, C);
        // Test that oldest frame is chosen
        assertEquals("Should get least recently used page when all pages used equally",A, mReplacementPolicy.choose(mPool));
    }

    @Test
    public void testChooseOutOfOrder() throws Exception {
        mReplacementPolicy.notify(mPool, A);
        mReplacementPolicy.notify(mPool, B);
        mReplacementPolicy.notify(mPool, A);
        mReplacementPolicy.notify(mPool, D);
        mReplacementPolicy.notify(mPool, E);
        mReplacementPolicy.notify(mPool, C);
        // Test that oldest frame is chosen
        assertEquals("Should get least recently used page when other pages loaded earlier but used subsequently",B, mReplacementPolicy.choose(mPool));
    }

    @After
    public void testNoChangeToList() {
        assertEquals("Pool size should not have changed",NUMFRAMES, mPool.size());
    }

    @Test
    public void testChooseOne() throws Exception {
        List<BufferFrame> pool = TestUtils.generateBufferFrameList(1);
        BufferFrame first = pool.get(0);
        for(int i = 0; i < 10; i++) {
            assertSame("Should always get the same frame if pool only has a single frame", first, mReplacementPolicy.choose(pool));
        }
    }
}