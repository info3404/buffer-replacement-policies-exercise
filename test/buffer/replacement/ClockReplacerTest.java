package buffer.replacement;

import buffer.BufferFrame;
import buffer.replacement.ClockReplacer;
import buffer.replacement.Replacer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Unit tests for Medium task
 * To get these tests to pass you will need to fully implement ClockReplacementPolicy
 * (@see ClockReplacementPolicy)
 */
public class ClockReplacerTest {

	private static final int POOLSIZE = 5;
	private Replacer replacer;
	private List<BufferFrame> pool;
	private HashMap<BufferFrame,String> labels; // Label for each buffer frame

	@Before
	public void setUp() throws Exception {
		replacer = new ClockReplacer();
		// Always start off with a pool of 5 frames    	
		pool = TestUtils.generateBufferFrameList(POOLSIZE);

		// NOTE: Changed so that we deal with frame labels rather
		// than addresses in the tests
		labels = new HashMap<BufferFrame,String>();
		labels.put(pool.get(0), "A");
		labels.put(pool.get(1), "B");
		labels.put(pool.get(2), "C");
		labels.put(pool.get(3), "D");
		labels.put(pool.get(4), "E");
	}

    @After
    public void testNoChangeToList() {
    	assertEquals("Pool size should not have changed",POOLSIZE, pool.size());
    }

	/*
	 * These tests check behaviour when there is a clear choice of page based 
	 * upon reference flag (set when accessed).
	 */

	@Test
	public void testChooseWhenOneFrameUnused() throws Exception {
		// Access all pages except the middle (C)
		TestUtils.notifyMany(replacer, pool, 1, 1, 0, 1, 1);
		assertEquals("Chosen frame should be the one containing the un-accessed page", "C", labels.get(replacer.choose(pool)));
	}
	
	/*
	 * These tests expect the the clock hand to progress one after choosing a page
	 */
	
    @Test
    public void testChooseFirst() throws Exception {
        TestUtils.notifyMany(replacer, pool, 1, 1, 1, 1, 1);
        assertEquals("First frame should be chosen if all accessed", "A", labels.get(replacer.choose(pool)));
    }
    
    @Test
    public void testChooseFirstAfterManyAccesses() throws Exception {
        TestUtils.notifyMany(replacer, pool, 5, 4, 3, 2, 1);
        assertEquals("First frame should be chosen if all accessed more than once", "A", labels.get(replacer.choose(pool)));
    }
    
    @Test
    public void testMoveToNextFrameAfterChosen() throws Exception {
    	BufferFrame chosen;
		int newPageId = POOLSIZE +1;
		TestUtils.notifyMany(replacer, pool, 0, 0, 0, 1, 0);
        assertEquals("First chosen frame should be the first in the pool", "A", labels.get(chosen = replacer.choose(pool)));
        
        TestUtils.simulatePageLoad(replacer, pool, chosen, newPageId++);
        assertEquals("Second chosen frame should be the second in the pool", "B", labels.get(chosen = replacer.choose(pool)));
        
        TestUtils.simulatePageLoad(replacer, pool, chosen, newPageId++);
        assertEquals("Third chosen frame should be the third in the pool", "C", labels.get(chosen = replacer.choose(pool)));
        
        TestUtils.simulatePageLoad(replacer, pool, chosen, newPageId++);
        assertEquals("Fourth chosen frame should be the fifth in the pool, passing the fifth frame since reference flag set", "E", labels.get(chosen = replacer.choose(pool)));
        
        TestUtils.simulatePageLoad(replacer, pool, chosen, newPageId++);
        assertEquals("Fifth chosen frame should be the fourth in the pool, passing the other frames since reference flag set", "D", labels.get(chosen = replacer.choose(pool)));
    }

	/*
	 * These tests check behaviour when some pages are pinned
	 */


	@Test
	public void testChooseAllFramesPinned() {
		for(BufferFrame frame: pool) {
			frame.pin();
		}

		boolean exceptionThrown = false;
		try {
			replacer.choose(pool);
		} catch (BufferFrame.AllBufferFramesPinnedException e) {
			exceptionThrown = true;
		}

		assertTrue("Choosing a page to replace when all pages pinned should throw an exception", exceptionThrown);
	}
	
  @Test
  public void testChooseClockWithPin() throws Exception {
	  BufferFrame chosen;
		int newPageId = POOLSIZE +1;
		TestUtils.notifyMany(replacer, pool, 1, 1, 1, 1, 1);

	  assertEquals("Clock hand starts at first frame", "A", labels.get(chosen = replacer.choose(pool)));
	  
	  TestUtils.simulatePageLoad(replacer, pool, chosen, newPageId++);
	  assertEquals("Clock hand progressed to second frame", "B", labels.get(chosen = replacer.choose(pool)));
	  
	  TestUtils.simulatePageLoad(replacer, pool, chosen, newPageId++);
	  chosen.pin(); // Keep page B pinned for rest of test
	  assertEquals("Clock hand progressed to third frame", "C", labels.get(chosen = replacer.choose(pool)));
	  
	  TestUtils.simulatePageLoad(replacer, pool, chosen, newPageId++);
	  assertEquals("Clock hand progressed to fourth frame", "D", labels.get(chosen = replacer.choose(pool)));
	  
	  TestUtils.simulatePageLoad(replacer, pool, chosen, newPageId++);
	  assertEquals("Clock hand progressed to first frame", "E", labels.get(chosen = replacer.choose(pool)));
	  
	  TestUtils.simulatePageLoad(replacer, pool, chosen, newPageId++);
	  assertEquals("Clock hand cycles back to first frame", "A", labels.get(chosen = replacer.choose(pool)));
	  
	  TestUtils.simulatePageLoad(replacer, pool, chosen, newPageId++);
	  assertEquals("Clock hand skips pinned second frame", "C", labels.get(chosen = replacer.choose(pool)));

  }

}