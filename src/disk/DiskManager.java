package disk;

import java.util.ArrayList;

/**
 * Trivial Disk Manager that doesn't actually handle data, but just tracks which
 * pages are accessed.
 * 
 * Usually a disk manager
 * - Manages access to the disk.
 * - Responsible for allocating/deallocating pages on file, and providing the methods to read/write a page
 *
 * Do note: This implementation does not keep track of deallocated pages.
 */
public class DiskManager {

	private ArrayList<Integer> pageAccessCounts = new ArrayList<Integer>();
    private int totalPageAccesses;


    /**
     * Returns the number of pages in the database
     */
    public long getNumPages() {
        return pageAccessCounts.size();
    }

    /**
     * Allocates a page on disk
     * @return the page id of the allocated page
     */
    public int allocatePage() {
    	int newPageId = pageAccessCounts.size();
    	pageAccessCounts.add(0);
        return newPageId;
    }

   
    /**
     * Reads the page from file 
     * @param pageId Id of page to read
     * @param page Page in memory into which page data is written
     */
    public void readPage(int pageId, Page page) {
    	updatePageAccess(pageId);
    	// Don't actually do any reading of data in this exercise
    }

    /**
     * Writes the page to disk
     * @param pageId Id of page to write
     * @param page Page in memory from which page data is written
     */
    public void writePage(Integer pageId, Page page) {
    	updatePageAccess(pageId);
    	// Don't actually do any writing of data in this exercise
    }

    /**
     * Returns the number of disk reads that have taken place
     */
    public int getPageAccesses() {
        return totalPageAccesses;
    }
    
    private void updatePageAccess(int pageId) {
        Integer pageAccessCount = pageAccessCounts.get(pageId);
        pageAccessCount++;
        totalPageAccesses++;
    }

	public void printStats() {
		StringBuilder builder = new StringBuilder("** Disk Manager (" + totalPageAccesses + "):");
		for(int pageId=0; pageId < pageAccessCounts.size(); ++pageId) {
			builder.append(" " + Integer.toString(pageId) + "(" + pageAccessCounts.get(pageId) + ")");
		}
		System.out.println(builder.toString());
	}
}
