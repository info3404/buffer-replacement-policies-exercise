package demo;
import buffer.BufferManager;
import buffer.replacement.*;
import disk.DiskManager;

import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Example Execution of Buffer Manager
 */
public class BufferManagerDemo {

    public static void main(String[] args) throws IOException {
    	
    	try(Scanner scanner = new Scanner(System.in)) {	    	
	    	Policy policy = askForPolicy(scanner);
	    	if(policy==Policy.GCLOCK) {
	    		gclockLimit = askForInt(scanner, "Count limit for GClock?", 1, null);
	    	}
	    		
	    	int nPages = askForInt(scanner, "How many pages in your disk?", 1, null);
	    	DiskManager dm = new DiskManager();
	    	for(int i=0; i<nPages; ++i)
	    		dm.allocatePage();
	    	
	    	Replacer replacer = policy.buildReplacer();
	    	
    		int nBuffers = askForInt(scanner, "How many frames in your buffer?", 1, null);
	    	BufferManager bm = new BufferManager(nBuffers, replacer, dm);
	    	
	    	Pattern pattern = Pattern.compile("(\\d+)([-\\+\\*]{0,2})");
	    	// Loop through allowing user to request pages, printing current state and stats
	    	while(true) {
	    		dm.printStats();
	    		bm.printStats();
	    		replacer.printStats();
	    		
	    		System.out.println("Which page would you like to access (append * to modify, +/- to add/remove pin, )?: ");
	    		if (scanner.hasNextLine()) {
	    			String line = scanner.nextLine();
	    			Matcher matcher = pattern.matcher(line);
	    			if(matcher.matches()) {
	    				int pageId = Integer.valueOf(matcher.group(1));
	    				bm.getPage(pageId); // Don't need to actually do anything with the page
	    				String option = matcher.group(2);
	    				if(option.contains("+")) {
	    					bm.pin(pageId);
	    				} else if (option.contains("-")) {
	    					bm.unpin(pageId);
	    				}
	    				if(option.contains("*"))
	    					bm.markDirty(pageId);
	    			} else {
		    			System.out.println("Sorry, can't parse that.");
		    		}
	    		}
	    	}
    	}
  }
    	
	public static int gclockLimit = 1;

	public enum Policy {
	    RANDOM,
	    MRU,
	    LRU,
	    CLOCK,
	    GCLOCK;
	    
	    Replacer buildReplacer() {
	        switch (this) {
	            case MRU:
	                return new MruReplacer();
	            case LRU:
	                return new LruReplacer();
	            case CLOCK:
	                return new ClockReplacer();
	            case GCLOCK:
	                return new GeneralisedClockReplacer(gclockLimit);
	            default: // RANDOM
	            	return new RandomReplacer();
	        }
	    }
	}

	/**
	 * @param scanner
	 * @param question
	 * @param minValue
	 * @param maxValue
	 * @return
	 */
	private static int askForInt(Scanner scanner, final String question, final Integer minValue,
			final Integer maxValue) {
		Integer value = null;
		while(value == null) {
			System.out.println(question + ": ");
			if (scanner.hasNextLine()) {
				try {
					int i = Integer.valueOf(scanner.nextLine());
					if (minValue!=null && i<minValue) {
						System.out.println("Sorry, the number needs to at least " + minValue);
					} else if (maxValue!=null && i>maxValue) {
						System.out.println("Sorry, the number needs to be no more than " + maxValue);
					} else {
						value = i;
					}
				} catch (NumberFormatException e){
					System.out.println("Sorry, you need to type an integer number");
				}
			}
		}
		return value;
	}

	/**
	 * Allow user to choose policy
	 * @param scanner Text console input scanner
	 * @return Chosen policy
	 */
	private static Policy askForPolicy(Scanner scanner) {
		Policy policy = null;
		while(policy == null) {
			System.out.println("Available policies are:");
			for (Policy p: Policy.values()) {
				System.out.println(p.toString());
			}
			System.out.println("Which buffer policy?: ");
			
			if (scanner.hasNextLine()) {
				String txt = scanner.nextLine();
				try {
					policy = Policy.valueOf(txt.toUpperCase());
				} catch (java.lang.IllegalArgumentException e) {
					System.out.println("Sorry, that policy isn't recognised");
					policy = null;
				}
			}
		}
		return policy;
	}

}
