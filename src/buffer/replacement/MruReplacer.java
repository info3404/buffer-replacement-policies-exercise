package buffer.replacement;

import java.util.List;

import buffer.BufferFrame;

/**
 * Most Recently Used Replacement Policy
 */
public class MruReplacer implements Replacer {
    @Override
    public String getName() {
        return "MRU";
    }

    @Override
    public BufferFrame choose(List<BufferFrame> pool) throws BufferFrame.AllBufferFramesPinnedException {
    	// HINT: If most recent page is always placed at the end of the list
    	// this method would just need to choose the end frame in pool.
    	throw new NotImplementedException();
	}

    @Override
    public void notify(List<BufferFrame> pool, BufferFrame frame) {
    	// HINT: to support the choose method, we could move the newly
    	// accessed frame to the end of pool.
    	throw new NotImplementedException();
    }
    
	@Override
	public void printStats() {
		// You can leave this untouched, or print something useful
		// using System.out.println
		
	}
}
