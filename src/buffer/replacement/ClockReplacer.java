package buffer.replacement;

import java.util.List;

import buffer.BufferFrame;

/**
 * Clock Replacement Policy
 */
public class ClockReplacer implements Replacer {

    @Override
    public String getName() {
        return "CLOCK";
    }

    @Override
    public BufferFrame choose(List<BufferFrame> pool) throws BufferFrame.AllBufferFramesPinnedException {
        throw new NotImplementedException();
    }

    @Override
    public void notify(List<BufferFrame> pool, BufferFrame frame) {
    	throw new NotImplementedException();
    }
    
	@Override
	public void printStats() {
		// You can leave this untouched, or print something useful
		// using System.out.println
		
	}
}
