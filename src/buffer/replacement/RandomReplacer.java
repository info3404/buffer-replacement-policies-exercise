package buffer.replacement;

import buffer.BufferFrame;

import java.util.List;
import java.util.Random;

/**
 * Random Replacement Policy
 * - Randomly selects a page from the pool for replacement
 */
public class RandomReplacer implements Replacer {

    private Random generator;

    public RandomReplacer() {
        generator = new Random();
    }

    @Override
    public String getName() {
        return "RANDOM POLICY";
    }

    @Override
    public BufferFrame choose(List<BufferFrame> pool) {
        assert pool.size() > 0 : "Expects a pool of at least size 1";
        // Pick a random index, try and get that frame
        int randomIndex = generator.nextInt(pool.size());
        return pool.get(randomIndex);
    }

    @Override
    public void notify(List<BufferFrame> pool, BufferFrame frame) {}

	@Override
	public void printStats() {
		// You can leave this untouched, or print something useful
		// using System.out.println
		
	}
}
