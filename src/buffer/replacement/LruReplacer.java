package buffer.replacement;

import java.util.List;

import buffer.BufferFrame;

/**
 * Least Recently Used Replacement Policy
 */
public class LruReplacer implements Replacer {

    @Override
    public String getName() {
        return "LRU ";
    }

    @Override
    public BufferFrame choose(List<BufferFrame> pool) {
    	throw new NotImplementedException();
    }

    @Override
    public void notify(List<BufferFrame> pool, BufferFrame frame) {
    	throw new NotImplementedException();
    }
    
	@Override
	public void printStats() {
		// You can leave this untouched, or print something useful
		// using System.out.println
		
	}
}
