package buffer.replacement;

import java.util.List;

import buffer.BufferFrame;

/**
 * Buffer frame replacer implementing GLOCK Replacement Policy
 */
public class GeneralisedClockReplacer implements Replacer {

    public GeneralisedClockReplacer(int limit) {
    }

    @Override
    public String getName() {
        return "GCLOCK Replacement";
    }

    @Override
    public BufferFrame choose(List<BufferFrame> pool) {
    	throw new NotImplementedException();
    }

    @Override
    public void notify(List<BufferFrame> pool, BufferFrame frame) {
    	throw new NotImplementedException();
    }
    
	@Override
	public void printStats() {
		// You can leave this untouched, or print something useful
		// using System.out.println
		
	}

}
