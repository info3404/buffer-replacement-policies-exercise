package buffer;

import disk.Page;

/**
 * Describes the contents of a buffer
 * Contains a Page and a PageId
 */
public class BufferFrame {
	public static final int INVALID_PAGE_ID = -1;
	
    private Page content;
    private Integer pageId;
    private boolean isDirty;
    private int pinned;

    public BufferFrame() {
        content = new Page();
        pageId = INVALID_PAGE_ID;
        isDirty = false;
        pinned = 0;
    }

    // checks whether current frame is valid or empty
    public boolean isEmpty() {
        return (this.pageId == null);
    }

    // checks whether the current frame is dirty (content changed) or not
    public boolean isDirty() {
        return this.isDirty;
    }

    // marks the current frame as dirty
    public void setDirty(boolean isDirty) {
        this.isDirty = isDirty;
    }

    // checks whether the current frame is pinned or not
    public boolean isPinned() {
        return (this.pinned > 0);
    }

    // marks the current frame as pinned (automatically done as part of getPage)
    public void pin() {
        this.pinned++;
    }

    // reduces the current frame's pin count (but not if it is already 0)
    public void unpin() {
        if (this.isPinned())
            this.pinned--;
    }

    // checks whether this frame contains a certain page
    public boolean contains(Integer pid) {
        return pageId.equals(pid);
    }

    public Integer getPageId() {
        return pageId;
    }

    // gets the Page content of current frame
    public Page getPage() {
        return this.content;
    }

    // stores a given Page in this frame
    public void setPage(Integer pid, Page page) {
        if (this.isDirty())
            throw new BufferFrameDirtyException();
        this.content.copy(page);
        this.pageId = pid;
        this.pinned = 0;
        this.isDirty = false;
    }

    public static class BufferFrameDirtyException extends RuntimeException {}
    public static class AllBufferFramesPinnedException extends RuntimeException {}
}
